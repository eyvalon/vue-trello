---
title: Description
created: "2020-06-25T15:38:33.957Z"
modified: "2020-06-27T02:44:26.019Z"
---

### Description

Creating a replication of a widely used collaboration tools, Trello using VueJs. I wanted to make this project as a result of improving my Front-End development skills and using VueJs and so, this will mainly be a focus on one board page.

To see the UI designs of this page, the reference can be found [here](https://bitbucket.org/eyvalon/ui-designs/src/master/Example%207/)

# From the website

![Alt Text](./images/new.png)

**Features implemented**:

- Allows dragging between each group for moving
- Deleting objects of list or entry via bin button
- Adding new entry to current list or new list
- Expandable to one page wide just like Trello
- Adding in options for left border options to make it dynamic in the case user wants to add custom colouring
- Storing the object data to LocalStorage for offline viewing (expiry is set to 60 seconds / 1 minute)
- Having a label for each board (i.e. current - Home Page with description)

**Features that was not implemented**:

- Editable text to change title / descriptions
- Priority colour bar (dynamically changing based on the level)
- Accepting the use of cookies policy (indicating that data will be used to store and changed accordingly to localstorage)
- **If we compare to the full features of a Trello, then there are much more list**

**Changes made while developing UI design and web design**

- Instead of the top border, I decided to go with the left border
- Removing the number of people icons, and instead replaced with left border (indicating which team it belongs to
- Having a button indication so that users know which one is clickable
- Priority level is set to be High (red), Medium (Yellow), Low (Green)
- As of 1st August 2020, I have redesigned to match the looks of trello. For reference, the image folder contains the old picture that was used as initial web design
- Refactoring code
- When in mobile, there will be option for bar icon used for pulling up sidebar

## Project setup

```
npm install
```

## Running project

To run on default port 8080:

```
npm run serve
```

To run on a certain port (i.e. 3000):

```
npm run serve -- --port 3000
```
