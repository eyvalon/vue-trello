import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/views/Home.vue";
import PageNotFound from "@/views/PageError.vue";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "Home",
      component: Home
    },
    {
      path: "/404",
      component: PageNotFound
    },

    { path: "*", redirect: "/404" }
  ]
});

export default router;
