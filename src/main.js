import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import { mutations } from "@/statics/data/store.js";

Vue.config.productionTip = false;

new Vue({
  router,
  beforeCreate() {
    mutations.getDataFromLocalStorage();
  },

  render: (h) => h(App)
}).$mount("#app");
