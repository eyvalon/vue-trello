import Vue from "vue";

export const store = Vue.observable({
  isNavOpen: false,
  listArray: [
    {
      id: 1,
      title: "Front-End Development",
      showMenu: false,
      newDataEntry: false,
      toDoList: [
        {
          id: 11,
          priority: "High Prioirty",
          borderColor: {
            borderColor: "red",
            backgroundColor: "red"
          },
          overrideColor: {
            borderColor: "purple"
          },
          description:
            " Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quo velit, at dicta tempora possimus sunt. Rem praesentium beatae quia repellat. Nihil excepturi sunt dolorum, veritatis illo repellat ut nam mollitia? "
        },
        {
          id: 12,
          priority: "Low Prioirty",
          borderColor: {
            borderColor: "green",
            backgroundColor: "green"
          },
          overrideColor: {
            borderColor: "green"
          },
          description:
            " Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quo velit, at dicta tempora possimus sunt. Rem praesentium beatae quia repellat. Nihil excepturi sunt dolorum, veritatis illo repellat ut nam mollitia? "
        }
      ],
      messageNewEntry: ""
    },
    {
      id: 2,
      title: "Back-End Development",
      showMenu: false,
      newDataEntry: false,
      toDoList: [
        {
          id: 1,
          priority: "High Prioirty",
          borderColor: {
            borderColor: "red",
            backgroundColor: "red"
          },
          overrideColor: {
            borderColor: "purple"
          },
          description:
            " Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quo velit, at dicta tempora possimus sunt. Rem praesentium beatae quia repellat. Nihil excepturi sunt dolorum, veritatis illo repellat ut nam mollitia? "
        }
      ],
      messageNewEntry: ""
    },
    {
      id: 3,
      title: "Server configurations",
      showMenu: false,
      newDataEntry: false,
      toDoList: [
        {
          id: 1,
          priority: "High Prioirty",
          borderColor: {
            borderColor: "red",
            backgroundColor: "red"
          },
          overrideColor: {
            borderColor: "purple"
          },
          description:
            " Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quo velit, at dicta tempora possimus sunt. Rem praesentium beatae quia repellat. Nihil excepturi sunt dolorum, veritatis illo repellat ut nam mollitia? "
        }
      ],
      messageNewEntry: ""
    }
  ]
});

export const mutations = {
  addNewRecord(newRecord, index) {
    store.listArray[index].toDoList.push(newRecord);
    store.listArray[index].newDataEntry = false;
    this.setData(false, index);
    this.saveDataToLocalStorage(store.listArray);
  },
  addNewTodo(newRecord) {
    store.listArray.push(newRecord);
    this.saveDataToLocalStorage(store.listArray);
  },
  setData(boolType, index) {
    store.listArray[index].newDataEntry = boolType;
  },
  removeItem(index, index1) {
    store.listArray[index].toDoList.splice(index1, 1);
    this.saveDataToLocalStorage(store.listArray);
  },
  removeTodo(index) {
    store.listArray.splice(index, 1);
    this.saveDataToLocalStorage(store.listArray);
  },
  getTodoList() {
    return store.listArray;
  },
  getDataFromLocalStorage() {
    const itemStr = localStorage.getItem("listArray");
    const item = JSON.parse(itemStr);

    if (item === null) {
      return;
    }

    const now = new Date();
    now.getTime() > item.expiry;

    if (now.getTime() > item.expiry) {
      localStorage.removeItem("listArray");
      return store.listArray;
    }

    store.listArray = item.listArray;
  },
  saveDataToLocalStorage(newRecord) {
    const now = new Date();
    let num = 60;
    const timeInSeconds = num * 1000;

    const item = {
      listArray: newRecord,
      expiry: now.getTime() + timeInSeconds
    };

    localStorage.setItem("listArray", JSON.stringify(item));
  },
  generateUniqueId() {
    return (
      "_" +
      Math.random()
        .toString(36)
        .substr(2, 4)
    );
  },
  setIsNavOpen(yesno) {
    store.isNavOpen = yesno;
  },
  toggleNav() {
    store.isNavOpen = !store.isNavOpen;
  }
};
