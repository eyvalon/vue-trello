import { store } from "@/statics/data/store.js";

export default {
  computed: {
    isPanelOpen() {
      return store.isNavOpen;
    }
  }
};
