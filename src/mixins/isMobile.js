export default {
  computed: {
    isMobile() {
      return this.windowWidth <= 768;
    }
  }
};
